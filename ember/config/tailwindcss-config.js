module.exports = {
  // mode: 'jit',
  purge: ['./app/**/*.html', './app/**/*.hbs'],
  darkMode: false, // or 'media' or 'class'
  theme: {
    fontFamily: {
      vt323: ['VT323'],
      delius: ['Delius'],
    },
    triangles: {
      // defaults to {}
      'right-up': {
        direction: 'right-up', // one of 'left', 'right', 'up', 'down', 'left-up', 'left-down', 'right-up', and 'right-down'
        size: '2em', // defaults to defaultSize
        height: '0.5em', // defaults to half the size; has no effect on the diagonal directions (e.g. 'left-up')
        color: 'currentColor', // defaults to defaultColor
      },
    },
    extend: {},
  },
  variants: {
    extend: {},
  },
  plugins: [
    require('@tailwindcss/forms'),
    require('@tailwindcss/aspect-ratio'),
    require('tailwindcss-triangles'),
  ],
};
